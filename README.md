# CssCheckBox for Vaadin 7 and 8

CssCheckBox is CSS based CheckBox component (Can you believe that?). At this point I would like to thank for the inspiration:
+ Eijsink Afrekensystemen BV for the [FormCheckBox](https://vaadin.com/directory#!addon/formcheckbox) addon. This addon showed me how easy is to change the DOM of a component. This addon was used as the base of my project.
+ Proto.io for the great CSS Switch. I used their generator to create the two basic CSS. So if you want to change, you should check this site how to do it: https://proto.io/freebies/onoff/

## Download release

Official releases of this add-on are available at Vaadin Directory. For Maven instructions, download and reviews, go to [CssCheckBox Add-on](https://vaadin.com/directory#!addon/csscheckbox-add-on)

## Build status

[![build-status](https://bitbucket-badges.useast.atlassian.io/badge/zolkiss/vaadin-csscheckbox.svg)](https://bitbucket.org/zolkiss/vaadin-csscheckbox/addon/pipelines/home)

## Building and running demo

  - git clone <url of the MyComponent repository>
  - mvn clean install
  - cd demo
  - mvn jetty:run

To see the demo, navigate to http://localhost:8080/

### Debugging server-side

If you have not already compiled the widgetset, do it now by running vaadin:install Maven target for CssCheckBox-root project.

If you have a JRebel license, it makes on the fly code changes faster. Just add JRebel nature to your CssCheckBox-demo project by clicking project with right mouse button and choosing JRebel > Add JRebel Nature

To debug project and make code modifications on the fly in the server-side, right-click the CssCheckBox-demo project and choose Debug As > Debug on Server. Navigate to http://localhost:8080/CssCheckBox-demo/ to see the application.

### Debugging client-side

Debugging client side code in the CssCheckBox-demo project:
  - run "mvn vaadin:run-codeserver" on a separate console while the application is running
  - activate Super Dev Mode in the debug window of the application or by adding ?superdevmode to the URL
  - You can access Java-sources and set breakpoints inside Chrome if you enable source maps from inspector settings.
 
## Release notes

### Version 1.0
+ CssCheckBox with three additional properties: Big preset, Simple mode, Animated

### Version 2.0
+ Changed supported Vaadin version to 8. To Vaadin 7, use the 1.0 version.

### Version 2.1
+ Added backward compatibility for Vaadin 7. For this version, use the org.vaadin.v7.* package.
## Issue tracking

The issues for this add-on are tracked on its github.com page. All bug reports and feature requests are appreciated. 

## Contributions

Contributions are welcome, but there are no guarantees that they are accepted as such. Process for contributing is the following:
- Fork this project
- Create an issue to this project about the contribution (bug or feature) if there is no such issue about it already. Try to keep the scope minimal.
- Develop and test the fix or functionality carefully. Only include minimum amount of code needed to fix the issue.
- Refer to the fixed issue in commit
- Send a pull request for the original project
- Comment on the original issue that you have implemented a fix for it

# Developer Guide

## Getting started

For using the component just simply create a new instance: new CssCheckBox();
The default size of the component is 60px * 24px and it is animated.

## Properties
The component has three additional properties:
- Big preset
- Simple mode
- Animated

### Big preset
This preset changes the dimension of the component to 100px * 40px. To apply this preset just call the ```setBigPreset(boolean bigPreset)``` method.

### Simple preset
The simple component is like a normal checkbox. There is no animation, and moving switch part. To use call the ```setSimpleMode(boolean simpleMode)``` method.

### Animated
This turns on-off the animation of component. Default this property is true. To turn off call the ```setAnimated(boolean animated)``` method.

## Recoloring the component
If we want to recolor the component, the two css selectors can be used:
1. ```.v-csscheckbox-inner:before``` - Select the OFF state of the component
2. ```.v-csscheckbox-inner:after``` - Select the ON state of the component

For changing the color simply change background-color and color properties. For example:
```css
.demo .recolored .v-csscheckbox-inner:after {
    background-color: palevioletred;
    color: palegreen;
}

.demo .recolored .v-csscheckbox-inner:before {
    background-color: palegreen;
    color: palevioletred;
}
```
 
Note: I already added a "recolored" style for the component, because I don't wanted to change every component.

For a more comprehensive example, src/main/java/org/vaadin/addons/demo/DemoUI.java

## License & Author

Add-on is distributed under Apache License 2.0. For license terms, see LICENSE.txt.

CssCheckBox is written by zolkiss