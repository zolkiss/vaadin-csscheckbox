package org.vaadin.addons.demo;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.data.HasValue;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.v7.data.Property;
import com.vaadin.v7.ui.Field;
import org.vaadin.addons.CssCheckBox;

import javax.servlet.annotation.WebServlet;

@Theme("demo")
@Title("MyComponent Add-on Demo")
@Widgetset("org.vaadin.addons.demo.DemoWidgetSet")
@SuppressWarnings("serial")
public class DemoUI extends UI {

  @WebServlet(value = "/*", asyncSupported = true)
  @VaadinServletConfiguration(productionMode = false, ui = DemoUI.class, widgetset = "org.vaadin.addons.demo.DemoWidgetSet")
  public static class Servlet extends VaadinServlet {
  }

  @Override
  protected void init(VaadinRequest request) {
    final GridLayout gridLayout = new GridLayout(4, 10);
    CheckBoxValueChangeListener valueChangeListener = new CheckBoxValueChangeListener();

    gridLayout.addComponent(new Label("Vaadin8"), 1, 0);
    gridLayout.addComponent(new Label("Vaadin7 Compatibility"), 2, 0);

    gridLayout.addComponent(new Label("Default CSS CheckBox"), 0, 1);
    gridLayout.addComponent(new Label("Big CSS CheckBox"), 0, 2);
    gridLayout.addComponent(new Label("Default Simple CSS CheckBox"), 0, 3);
    gridLayout.addComponent(new Label("Big simple CSS CheckBox"), 0, 4);
    gridLayout.addComponent(new Label("Big simple CSS CheckBox"), 0, 5);
    gridLayout.addComponent(new Label("Not animated CSS CheckBox"), 0, 6);
    gridLayout.addComponent(new Label("Not animated Big CSS CheckBox"), 0, 7);
    gridLayout.addComponent(new Label("Big simple CSS CheckBox"), 0, 8);
    gridLayout.addComponent(new Label("Styled CSS CheckBox"), 0, 9);

    gridLayout.addComponent(new Label(), 3, 1);
    gridLayout.addComponent(new Label(), 3, 2);
    gridLayout.addComponent(new Label(), 3, 3);
    gridLayout.addComponent(new Label(), 3, 4);
    gridLayout.addComponent(new Label(), 3, 5);
    gridLayout.addComponent(new Label(), 3, 6);
    gridLayout.addComponent(new Label(), 3, 7);
    gridLayout.addComponent(new Label(), 3, 8);
    gridLayout.addComponent(new Label(), 3, 9);

    gridLayout.setComponentAlignment(gridLayout.getComponent(1, 0), Alignment.MIDDLE_CENTER);
    gridLayout.setComponentAlignment(gridLayout.getComponent(2, 0), Alignment.MIDDLE_CENTER);

    gridLayout.addComponent(getDefault(valueChangeListener), 1, 1);
    gridLayout.addComponent(getBigNormal(valueChangeListener), 1, 2);
    gridLayout.addComponent(getSimple(valueChangeListener), 1, 3);
    gridLayout.addComponent(getBigSimple(valueChangeListener), 1, 4);
    gridLayout.addComponent(getNotAnimatedNormal(valueChangeListener), 1, 5);
    gridLayout.addComponent(getNotAnimatedBigNormal(valueChangeListener), 1, 6);
    gridLayout.addComponent(getNotAnimatedSimple(valueChangeListener), 1, 7);
    gridLayout.addComponent(getNotAnimatedBigSimple(valueChangeListener), 1, 8);
    gridLayout.addComponent(getStyles(valueChangeListener), 1, 9);

    gridLayout.addComponent(getDefaultOld(valueChangeListener), 2, 1);
    gridLayout.addComponent(getBigNormalOld(valueChangeListener), 2, 2);
    gridLayout.addComponent(getSimpleOld(valueChangeListener), 2, 3);
    gridLayout.addComponent(getBigSimpleOld(valueChangeListener), 2, 4);
    gridLayout.addComponent(getNotAnimatedNormalOld(valueChangeListener), 2, 5);
    gridLayout.addComponent(getNotAnimatedBigNormalOld(valueChangeListener), 2, 6);
    gridLayout.addComponent(getNotAnimatedSimpleOld(valueChangeListener), 2, 7);
    gridLayout.addComponent(getNotAnimatedBigSimpleOld(valueChangeListener), 2, 8);
    gridLayout.addComponent(getStylesOld(valueChangeListener), 2, 9);

    for (int i = 0; i <= 3; i++) {
      for (int j = 1; j <= 9; j++) {
        gridLayout.setComponentAlignment(gridLayout.getComponent(i, j), Alignment.MIDDLE_CENTER);
      }
    }


    gridLayout.setHeight(100, Unit.PERCENTAGE);
    gridLayout.setWidth(700, Unit.PIXELS);

    VerticalLayout mainLayout = new VerticalLayout();
    mainLayout.addComponent(gridLayout);

    mainLayout.setComponentAlignment(gridLayout, Alignment.TOP_CENTER);
    mainLayout.setStyleName("demoContentLayout");
    mainLayout.setSizeFull();

    setContent(mainLayout);

  }

  private CssCheckBox getStyles(CheckBoxValueChangeListener valueChangeListener) {
    final CssCheckBox stylesCssCheckBox = new CssCheckBox();
    stylesCssCheckBox.addValueChangeListener(valueChangeListener);
    stylesCssCheckBox.addStyleName("recolored");
    return stylesCssCheckBox;
  }

  private CssCheckBox getNotAnimatedBigSimple(CheckBoxValueChangeListener valueChangeListener) {
    final CssCheckBox notAnimatedBigSimpleCssCheckBox = new CssCheckBox();
    notAnimatedBigSimpleCssCheckBox.setBigPreset(true);
    notAnimatedBigSimpleCssCheckBox.setSimpleMode(true);
    notAnimatedBigSimpleCssCheckBox.setAnimated(false);
    notAnimatedBigSimpleCssCheckBox.addValueChangeListener(valueChangeListener);
    return notAnimatedBigSimpleCssCheckBox;
  }

  private CssCheckBox getNotAnimatedSimple(CheckBoxValueChangeListener valueChangeListener) {
    final CssCheckBox notAnimatedSimpleCssCheckBox = new CssCheckBox();
    notAnimatedSimpleCssCheckBox.setSimpleMode(true);
    notAnimatedSimpleCssCheckBox.setAnimated(false);
    notAnimatedSimpleCssCheckBox.addValueChangeListener(valueChangeListener);
    return notAnimatedSimpleCssCheckBox;
  }

  private CssCheckBox getNotAnimatedBigNormal(CheckBoxValueChangeListener valueChangeListener) {
    final CssCheckBox notAnimatedBigNormalCssCheckBox = new CssCheckBox();
    notAnimatedBigNormalCssCheckBox.setBigPreset(true);
    notAnimatedBigNormalCssCheckBox.setAnimated(false);
    notAnimatedBigNormalCssCheckBox.setEnabled(false);
    notAnimatedBigNormalCssCheckBox.setValue(Boolean.TRUE);
    notAnimatedBigNormalCssCheckBox.addValueChangeListener(valueChangeListener);
    return notAnimatedBigNormalCssCheckBox;
  }

  private CssCheckBox getNotAnimatedNormal(CheckBoxValueChangeListener valueChangeListener) {
    final CssCheckBox notAnimatedNormalCssCheckBox = new CssCheckBox();
    notAnimatedNormalCssCheckBox.setAnimated(false);
    notAnimatedNormalCssCheckBox.addValueChangeListener(valueChangeListener);
    return notAnimatedNormalCssCheckBox;
  }

  private CssCheckBox getBigSimple(CheckBoxValueChangeListener valueChangeListener) {
    final CssCheckBox bigSimpleCssCheckBox = new CssCheckBox();
    bigSimpleCssCheckBox.setBigPreset(true);
    bigSimpleCssCheckBox.setSimpleMode(true);
    bigSimpleCssCheckBox.setEnabled(false);
    bigSimpleCssCheckBox.addValueChangeListener(valueChangeListener);
    return bigSimpleCssCheckBox;
  }

  private CssCheckBox getSimple(CheckBoxValueChangeListener valueChangeListener) {
    final CssCheckBox simpleCssCheckBox = new CssCheckBox();
    simpleCssCheckBox.setSimpleMode(true);
    simpleCssCheckBox.addValueChangeListener(valueChangeListener);
    return simpleCssCheckBox;
  }

  private CssCheckBox getBigNormal(CheckBoxValueChangeListener valueChangeListener) {
    final CssCheckBox bigNormalCssCheckBox = new CssCheckBox();
    bigNormalCssCheckBox.setBigPreset(true);
    bigNormalCssCheckBox.addValueChangeListener(valueChangeListener);
    return bigNormalCssCheckBox;
  }

  private CssCheckBox getDefault(CheckBoxValueChangeListener valueChangeListener) {
    final CssCheckBox normalCssCheckBox = new CssCheckBox();
    normalCssCheckBox.addValueChangeListener(valueChangeListener);
    return normalCssCheckBox;
  }

  private org.vaadin.v7.addons.CssCheckBox getNotAnimatedBigSimpleOld(CheckBoxValueChangeListener valueChangeListener) {
    final org.vaadin.v7.addons.CssCheckBox notAnimatedBigSimpleCssCheckBox = new org.vaadin.v7.addons.CssCheckBox();
    notAnimatedBigSimpleCssCheckBox.setBigPreset(true);
    notAnimatedBigSimpleCssCheckBox.setSimpleMode(true);
    notAnimatedBigSimpleCssCheckBox.setAnimated(false);
    notAnimatedBigSimpleCssCheckBox.addValueChangeListener(valueChangeListener);
    return notAnimatedBigSimpleCssCheckBox;
  }

  private org.vaadin.v7.addons.CssCheckBox getNotAnimatedSimpleOld(CheckBoxValueChangeListener valueChangeListener) {
    final org.vaadin.v7.addons.CssCheckBox notAnimatedSimpleCssCheckBox = new org.vaadin.v7.addons.CssCheckBox();
    notAnimatedSimpleCssCheckBox.setSimpleMode(true);
    notAnimatedSimpleCssCheckBox.setAnimated(false);
    notAnimatedSimpleCssCheckBox.addValueChangeListener(valueChangeListener);
    return notAnimatedSimpleCssCheckBox;
  }

  private org.vaadin.v7.addons.CssCheckBox getNotAnimatedBigNormalOld(CheckBoxValueChangeListener valueChangeListener) {
    final org.vaadin.v7.addons.CssCheckBox notAnimatedBigNormalCssCheckBox = new org.vaadin.v7.addons.CssCheckBox();
    notAnimatedBigNormalCssCheckBox.setBigPreset(true);
    notAnimatedBigNormalCssCheckBox.setAnimated(false);
    notAnimatedBigNormalCssCheckBox.setEnabled(false);
    notAnimatedBigNormalCssCheckBox.setValue(Boolean.TRUE);
    notAnimatedBigNormalCssCheckBox.addValueChangeListener(valueChangeListener);
    return notAnimatedBigNormalCssCheckBox;
  }

  private org.vaadin.v7.addons.CssCheckBox getNotAnimatedNormalOld(CheckBoxValueChangeListener valueChangeListener) {
    final org.vaadin.v7.addons.CssCheckBox notAnimatedNormalCssCheckBox = new org.vaadin.v7.addons.CssCheckBox();
    notAnimatedNormalCssCheckBox.setAnimated(false);
    notAnimatedNormalCssCheckBox.addValueChangeListener(valueChangeListener);
    return notAnimatedNormalCssCheckBox;
  }

  private org.vaadin.v7.addons.CssCheckBox getBigSimpleOld(CheckBoxValueChangeListener valueChangeListener) {
    final org.vaadin.v7.addons.CssCheckBox bigSimpleCssCheckBox = new org.vaadin.v7.addons.CssCheckBox();
    bigSimpleCssCheckBox.setBigPreset(true);
    bigSimpleCssCheckBox.setSimpleMode(true);
    bigSimpleCssCheckBox.setEnabled(false);
    bigSimpleCssCheckBox.addValueChangeListener(valueChangeListener);
    return bigSimpleCssCheckBox;
  }

  private org.vaadin.v7.addons.CssCheckBox getSimpleOld(CheckBoxValueChangeListener valueChangeListener) {
    final org.vaadin.v7.addons.CssCheckBox simpleCssCheckBox = new org.vaadin.v7.addons.CssCheckBox();
    simpleCssCheckBox.setSimpleMode(true);
    simpleCssCheckBox.addValueChangeListener(valueChangeListener);
    return simpleCssCheckBox;
  }

  private org.vaadin.v7.addons.CssCheckBox getBigNormalOld(CheckBoxValueChangeListener valueChangeListener) {
    final org.vaadin.v7.addons.CssCheckBox bigNormalCssCheckBox = new org.vaadin.v7.addons.CssCheckBox();
    bigNormalCssCheckBox.setBigPreset(true);
    bigNormalCssCheckBox.addValueChangeListener(valueChangeListener);
    return bigNormalCssCheckBox;
  }

  private org.vaadin.v7.addons.CssCheckBox getDefaultOld(CheckBoxValueChangeListener valueChangeListener) {
    final org.vaadin.v7.addons.CssCheckBox normalCssCheckBox = new org.vaadin.v7.addons.CssCheckBox();
    normalCssCheckBox.addValueChangeListener(valueChangeListener);
    return normalCssCheckBox;
  }

  private org.vaadin.v7.addons.CssCheckBox getStylesOld(CheckBoxValueChangeListener valueChangeListener) {
    final org.vaadin.v7.addons.CssCheckBox stylesCssCheckBox = new org.vaadin.v7.addons.CssCheckBox();
    stylesCssCheckBox.addValueChangeListener(valueChangeListener);
    stylesCssCheckBox.addStyleName("recolored");
    return stylesCssCheckBox;
  }

  private static class CheckBoxValueChangeListener implements HasValue.ValueChangeListener<Boolean>, Property.ValueChangeListener {

    public CheckBoxValueChangeListener() {
    }

    @Override
    public void valueChange(HasValue.ValueChangeEvent<Boolean> valueChangeEvent) {
      Component component = valueChangeEvent.getComponent();
      GridLayout parent = (GridLayout) component.getParent();
      int rowNum = parent.getComponentArea(component).getRow1();
      ((Label) parent.getComponent(3, rowNum)).setValue("V8-" + valueChangeEvent.getValue());
    }

    @Override
    public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
      org.vaadin.v7.addons.CssCheckBox component = (org.vaadin.v7.addons.CssCheckBox) ((Field.ValueChangeEvent) valueChangeEvent).getSource();
      GridLayout parent = (GridLayout) component.getParent();
      int rowNum = parent.getComponentArea(component).getRow1();
      ((Label) parent.getComponent(3, rowNum)).setValue("V7-" + component.getValue());
    }
  }

}
