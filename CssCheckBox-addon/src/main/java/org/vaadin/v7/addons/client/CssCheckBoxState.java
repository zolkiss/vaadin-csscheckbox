package org.vaadin.v7.addons.client;

import com.vaadin.shared.annotations.DelegateToWidget;
import com.vaadin.v7.shared.ui.checkbox.CheckBoxState;

/**
 * State class for the addon. It defines the basic class
 *
 * @author zolkiss
 */
public class CssCheckBoxState extends CheckBoxState {
  @DelegateToWidget
  private boolean bigPreset = false;
  @DelegateToWidget
  private boolean simpleMode = false;
  @DelegateToWidget
  private boolean animated = true;

  {
    primaryStyleName = "v-csscheckbox";
  }

  public boolean isBigPreset() {
    return bigPreset;
  }

  public void setBigPreset(boolean bigPreset) {
    this.bigPreset = bigPreset;
  }

  public boolean isSimpleMode() {
    return simpleMode;
  }

  public void setSimpleMode(boolean simpleMode) {
    this.simpleMode = simpleMode;
  }

  public boolean isAnimated() {
    return animated;
  }

  public void setAnimated(boolean animated) {
    this.animated = animated;
  }
}