package org.vaadin.addons;

import com.vaadin.ui.CheckBox;
import org.vaadin.addons.client.CssCheckBoxState;

/**
 * Default class.
 * It contains plus one method to add additional formatting options.
 *
 * @author zolkiss
 */
public class CssCheckBox extends CheckBox {

	public CssCheckBox(){initStyle();}

	public CssCheckBox(String caption) {
		super(caption);
		initStyle();
	}

	public CssCheckBox(String caption, boolean initialState) {
		super(caption, initialState);
		initStyle();
	}

	protected void initStyle(){}

	@Override
	protected CssCheckBoxState getState() {
		return (CssCheckBoxState) super.getState();
	}

	public boolean isSimpleMode() {
		return getState().isSimpleMode();
	}

	public void setSimpleMode(boolean simpleMode) {
		getState().setSimpleMode(simpleMode);
	}

	public boolean isBigPreset() {
		return getState().isBigPreset();
	}

	public void setBigPreset(boolean BigPreset) {
		getState().setBigPreset(BigPreset);
	}

	public boolean isAnimated() {
		return getState().isAnimated();
	}

	public void setAnimated(boolean animated) {
		getState().setAnimated(animated);
	}
}
