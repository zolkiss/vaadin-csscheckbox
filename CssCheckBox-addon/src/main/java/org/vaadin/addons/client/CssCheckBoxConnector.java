package org.vaadin.addons.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.checkbox.CheckBoxConnector;
import com.vaadin.shared.ui.Connect;
import org.vaadin.addons.CssCheckBox;

/**
 * Connector class for CssCheckBox.
 * It defines the added inner elements, and set the class for the already available classes.
 *
 * @author zolkiss
 */
@Connect(CssCheckBox.class)
public class CssCheckBoxConnector extends CheckBoxConnector {
	private static final String INNER_CONTENT = "<span class=\"v-csscheckbox-inner\"></span><span class=\"v-csscheckbox-switch\"></span>";
	private static final String BIG_PRESET_CLASS_NAME = "csscheckbox-big";
	private static final String SIMPLE_MODE_CLASS_NAME = "simple-button";
	private static final String ANIMATED_CLASS_NAME = "animated";

	public CssCheckBoxConnector() {
	}

	@Override
	protected void init() {
		super.init();

		Element inputElement = getWidget().getElement().getFirstChildElement();
		//Add custom class for the inner checkbox type input tag
		inputElement.addClassName("v-csscheckbox-checkbox");
		//Ass custom class for the inner label tag
		inputElement.getNextSiblingElement().addClassName("v-csscheckbox-label");
	}

	/**
	 * The method, which enables the label for the layout.
	 * It makes possible for the FormLayout to move the caption from the right of the controller to the left column.
	 * It is based on the FormCheckBox addon. See the link in the README.md
	 * @return
	 */
	public boolean delegateCaptionHandling() {
		return true;
	}

	public void onStateChanged(StateChangeEvent stateChangeEvent) {
		super.onStateChanged(stateChangeEvent);

		//Handling preset classes
		if (getState().isBigPreset()) {
			getWidget().addStyleName(BIG_PRESET_CLASS_NAME);
		} else {
			getWidget().removeStyleName(BIG_PRESET_CLASS_NAME);
		}

		if (getState().isSimpleMode()){
			getWidget().addStyleName(SIMPLE_MODE_CLASS_NAME);
		} else {
			getWidget().removeStyleName(SIMPLE_MODE_CLASS_NAME);
		}

		if (getState().isAnimated()){
			getWidget().addStyleName(ANIMATED_CLASS_NAME);
		} else {
			getWidget().removeStyleName(ANIMATED_CLASS_NAME);
		}
		//We prevent any text input in the inner label
		this.getWidget().setText((String) null);
		//Instead of the text input, we set the HTML content, which contains the spans to make possible of the using the CSS generator from proto.io
		this.getWidget().setHTML(INNER_CONTENT);
	}

	@Override
	public void onClick(ClickEvent event) {
		super.onClick(event);

		Element inputElement = getWidget().getElement().getFirstChildElement();
		//On click we manually add the checked tag to the checkbox input. This attribute is used by the CSS
		if(getState().checked){
			inputElement.setAttribute("checked", "true");
		} else {
			inputElement.removeAttribute("checked");
		}
	}

	@Override
	public CssCheckBoxState getState() {
		return (CssCheckBoxState) super.getState();
	}
}
